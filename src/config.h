#ifndef CONFIG_H
#define CONFIG_H


struct Config {

    Config() = default;
        
    const double c0 = 0.1;
    static constexpr const int grid_size = 1000;
    const double dx = 0.0001;
    const double dt = 0.01;

    const int t_max = 10;
    const double d = 1e-4;

    const int thread_number = 8;

    const int subarray_size  = grid_size/thread_number;

};
#endif