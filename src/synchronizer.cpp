/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Synchronizer.cpp
 * Author: pawel
 * 
 * Created on March 15, 2019, 9:19 PM
 */

#include <fstream>
#include "synchronizer.h"
#include <algorithm>

bool Synchronizer::isAllFinished() {    
    return std::find_if(computator.chunks.begin(), computator.chunks.end(), 
        [](const Chunk& ch) {return ch.done.load() == false;}) == computator.chunks.end();
}

void Synchronizer::syncNextStep() {
    bool ready_to_copy {false};
    while (!isAllFinished()) {        
        ready_to_copy = std::find_if_not(computator.chunks.begin(), computator.chunks.end(), 
            [](const Chunk& ch) {return ch.part_done.load();}) == computator.chunks.end();
        if (ready_to_copy) {
            LOG4CPLUS_DEBUG(logger, "Ready to copy, time step: " << time_step);
            for (auto &ch : computator.chunks) {
                LOG4CPLUS_DEBUG(logger, "Ready2copy Chunk: "<< ch.id << ", done: " << ch.part_done.load());                
            }            
            computator.grid = computator.grid_tmp;
            computator.setChunksFalse();
            computator.cv_sync.notify_all();            
            time_step++;
        }
        else {
            for (auto &ch : computator.chunks) {
                LOG4CPLUS_DEBUG(logger, "Not Ready2copy Chunk: "<< ch.id << ", done: " << ch.part_done.load());                
            }                        
            LOG4CPLUS_DEBUG(logger, "Not ready to copy, time step: " << time_step);
        }
    }
    
    LOG4CPLUS_DEBUG(logger, "Computation finished");
    std::ofstream myfile("output.txt");
    int vsize = computator.grid.size();
    for (int n=0; n < vsize; n++) {
        myfile << computator.grid[n] << std::endl;
    }        
    LOG4CPLUS_DEBUG(logger, "File saved");
}

Synchronizer::~Synchronizer() {
}
