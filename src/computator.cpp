#include "computator.h"
#include <iostream>

std::vector<Chunk> Computator::init_chunks() {
    std::vector<Chunk> chunk_tmp;    
    for (int i = 0; i < cfg.thread_number; i++) {
        Chunk ch {i, i * cfg.subarray_size, (i + 1) * cfg.subarray_size - 1};
        chunk_tmp.push_back(ch);        
    }
    return chunk_tmp;
}

double Computator::get_laplasian(int idx) {
    return (grid[idx + 1] - 2 * grid[idx] + grid[idx - 1])/(2 * cfg.dx);
}

double Computator::get_laplasian_left(int chunk_id, int idx_left) {
    LOG4CPLUS_DEBUG(logger, "Locking left of chunk = " << chunk_id);
    std::lock_guard<std::mutex> lock(mtx_vec[chunk_id - 1]);
    return (grid[idx_left + 1] - 2 * grid[idx_left] + grid[idx_left - 1])/(2 * cfg.dx);
}

double Computator::get_laplasian_right(int chunk_id, int idx_right) {
    std::lock_guard<std::mutex> lock(mtx_vec[chunk_id]);
    LOG4CPLUS_DEBUG(logger, "Locking right of chunk = " << chunk_id);
    return (grid[idx_right + 1] - 2 * grid[idx_right] + grid[idx_right - 1])/(2 * cfg.dx);
}

double Computator::get_laplasian_left_end() {
    return (grid[1] -  grid[0])/cfg.dx;
}

double Computator::get_laplasian_right_end() {
    return (grid[cfg.grid_size - 1] - grid[cfg.grid_size - 2])/cfg.dx;
}

void Computator::compute_chunk(Chunk& ch) {          
    if (ch.idx_left == 0) {
        grid_tmp[ch.idx_left] = cfg.d * get_laplasian_left_end() * cfg.dt + grid[ch.idx_left];
    }
    else {        
        grid_tmp[ch.idx_left] = cfg.d * get_laplasian_left(ch.id, ch.idx_left) * cfg.dt + grid[ch.idx_left];
    }
        
    for (int idx = ch.idx_left + 1; idx < ch.idx_right - 1; idx++) {
        grid_tmp[idx] = cfg.d * get_laplasian(idx) * cfg.dt + grid[idx]; 
    }
    
    if (ch.idx_right == cfg.grid_size - 1) {
        grid_tmp[ch.idx_right] = cfg.d * get_laplasian_right_end() * cfg.dt + grid[ch.idx_right];
    }
    else {
        grid_tmp[ch.idx_right] = cfg.d * get_laplasian_right(ch.id, ch.idx_right) * cfg.dt + grid[ch.idx_right];
    }
    ch.part_done = true;
}

//void Computator::run_chunk(int idx) {    
void Computator::run_chunk(Chunk& ch) {    
//    Chunk& ch = chunks[idx];
    for (int t = 0; t < cfg.t_max; t++) {        
        LOG4CPLUS_DEBUG(logger, "Computing chunk = " << ch.id << ", t = " << t);
        compute_chunk(ch);
        std::unique_lock<std::mutex> lock_sync(mtx_sync);
        LOG4CPLUS_DEBUG(logger, "Waiting for unlock chunk = " << ch.id << ", t = " << t);
        cv_sync.wait(lock_sync);
    }
    ch.done = true;        
}

void Computator::setChunksFalse() {
    for (auto& ch : chunks) {
        ch.part_done = false;
    }    
}

void Computator::run() {
    
    std::vector<std::thread> threads;
    for (int i = 0; i < cfg.thread_number; i++) {
        std::thread t(&Computator::run_chunk, this, std::ref(chunks[i]));
        threads.push_back(std::move(t));
    }
    for (auto& t : threads) {
        t.detach();
    }
}
