#include <iostream>
#include <array>
#include <chrono>
#include <cmath>
#include <thread>

#include "logger.h"
#include "computator.h"
#include "synchronizer.h"


int main() {

	auto start = std::chrono::high_resolution_clock::now();

        const Config cfg {};
        
	// array grid {}; 
	// grid[0] = c0;

	// array temp_grid = {};
	// temp_grid[0] = c0;

	// int time_steps = std::floor(t_max / dt);
	// std::cout << "Number of time-steps: " << (time_steps / 100) << " k" << std::endl ;

	// for (double t = 0; t < t_max; t += dt) {
	// 	for (int idx = 1; idx < grid_size - 2; idx++) {
	// 		temp_grid[idx] = d * get_laplasian(grid, idx) * dt + grid[idx]; 
	// 	}
	// 	temp_grid[grid_size - 1] = d * get_laplasian_end(grid) * dt + grid[grid_size - 1];
	// 	grid = temp_grid;

	// 	if ((static_cast<int>(std::floor(t)) % 100) == 0) {
	// 		std::cout << "t = " << t << std::endl;
	// 	}
	// }
	// show(grid);
        
        
        log4cplus::initialize();
        log4cplus::ConfigureAndWatchThread configureThread(LOG4CPLUS_TEXT("log4cplus.properties"), 5 * 1000);
        
        log4cplus::Logger logger = log4cplus::Logger::getInstance( LOG4CPLUS_TEXT("main"));        
        LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Hello, World!"));        
        
	Computator computator {cfg};
        computator.run();
                        
        Synchronizer sync {computator};
        std::thread th {&Synchronizer::syncNextStep, &sync};
        th.join();
                        
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << " s\n";
} 
