/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Synchronizer.h
 * Author: pawel
 *
 * Created on March 15, 2019, 9:19 PM
 */

#ifndef SYNCHRONIZER_H
#define SYNCHRONIZER_H

#include "config.h"
#include <vector>
#include <atomic>

#include "computator.h"
#include "logger.h"


class Synchronizer {
public:
    Synchronizer(Computator& comp) :
        computator{comp}
        {}
           
    virtual ~Synchronizer();
    void syncNextStep();
    bool isAllFinished();
               
private:        
    Computator& computator;
    int time_step {0};
    
    const log4cplus::Logger logger = log4cplus::Logger::getInstance("Synchronizer");
};

#endif /* SYNCHRONIZER_H */

