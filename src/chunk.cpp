/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   chunk.cpp
 * Author: pawel
 * 
 * Created on March 19, 2019, 5:21 PM
 */

#include "chunk.h"

Chunk::Chunk(const Chunk& c) {
    id = c.id;
    idx_left = c.idx_left;
    idx_right = c.idx_right;
    part_done = c.part_done.load();
    done = c.done.load();     
}



