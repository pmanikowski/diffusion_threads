#ifndef COMPUTATOR_H
#define COMPUTATOR_H

#include <thread>
#include <condition_variable>
#include <memory>
#include <vector>
#include <array>

#include "config.h"
#include "chunk.h"
#include "logger.h"


class Computator {
public:
	Computator(const Config& c) :
                    cfg {c},
                    chunks{init_chunks()} {
                        grid[0] = cfg.c0;
                    }

        void compute_chunk(Chunk& ch);        
//        void run_chunk(int idx);
        void run_chunk(Chunk& ch);
        
        std::vector<Chunk> init_chunks();
        
        const Config& cfg;
 
        std::vector<double> grid = std::vector<double>(cfg.grid_size, 0);
        std::vector<double> grid_tmp = std::vector<double>(cfg.grid_size, 0);;
        
        std::vector<std::mutex> mtx_vec = std::vector<std::mutex>(cfg.grid_size - 1);
        std::vector<std::mutex> mtx_vec_sync = std::vector<std::mutex>(cfg.grid_size);
        
        
        std::condition_variable cv_sync {};
        std::mutex mtx_sync {};
        
        double get_laplasian(int idx);
        double get_laplasian_left(int chunk_id, int idx_left);
        double get_laplasian_right(int chunk_id, int idx_right);
        double get_laplasian_left_end();
        double get_laplasian_right_end();

        std::vector<Chunk> chunks;
        
        void run();        
        bool next_step = false;
        
        void setChunksFalse();
        
        const log4cplus::Logger logger = log4cplus::Logger::getInstance("Computator");
};

#endif