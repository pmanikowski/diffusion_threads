/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   chunk.h
 * Author: pawel
 *
 * Created on March 19, 2019, 5:21 PM
 */

#ifndef CHUNK_H
#define CHUNK_H

#include <atomic>

struct Chunk {
    Chunk(int _id,
          int _idx_left,
          int _idx_right) :
    id {_id},
    idx_left {_idx_left},
    idx_right {_idx_right} {}
    
    Chunk(const Chunk& c);
            
    int id;
    int idx_left;
    int idx_right;
    std::atomic<bool> part_done {false};
    std::atomic<bool> done {false};
};
#endif /* CHUNK_H */

